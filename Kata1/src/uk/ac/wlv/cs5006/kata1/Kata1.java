package uk.ac.wlv.cs5006.kata1;

import java.util.Arrays;

/**
 * 
 * @author SujamathyS.
 * @param stringOne,stringTwo
 * @return boolean
 *
 */
public class Kata1 {

    /**
     * check whether two input strings are anagram, it will return boolean according to this.
     */
    public static boolean areAnagrams(String stringOne, String stringTwo) {
        if (stringOne != null && !stringOne.matches("[0-9]+") 
                && stringTwo != null && !stringTwo.matches("[0-9]+")) {
            // Converting both the string to lower case.
            String text1 = stringOne.toLowerCase();
            String text2 = stringOne.toLowerCase();

            // Checking for the length of strings
            if (text1.length() != text2.length()) {
                return false;
            } else {
                // Converting both the arrays to character array
                char[] string1 = stringOne.toCharArray();
                char[] string2 = stringTwo.toCharArray();

                // Sorting the arrays using in-built function sort ()
                Arrays.sort(string1);
                Arrays.sort(string2);

                // Comparing both the arrays using in-built function equals ()
                if (Arrays.equals(string1, string2) == true) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }
}
