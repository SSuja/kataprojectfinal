package uk.ac.wlv.cs5006.kata2;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
/**
 * 
 * @author SujamathyS.
 * @param  expression,String
 * @return double
 *
 */
public class Kata2 {
    /**
     * this method evaluate mathematical expressions.
     */
    public static double evaluate(String expression) throws NumberFormatException, ScriptException {
        if (expression != null && !expression.isEmpty()) {
            ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
            ScriptEngine engine = scriptEngineManager.getEngineByName("JavaScript");
            return Double.parseDouble((String) engine.eval(expression).toString());
        }
        return 0;
    }
}
