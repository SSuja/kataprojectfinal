package uk.ac.wlv.cs5006.kata2;

import javax.script.ScriptException;

public class Kata2Main {

    public static void main(String[] args) throws NumberFormatException, ScriptException {
        Kata2 kata2 = new Kata2();
        System.out.println(kata2.evaluate("40+2"));
    }

}
