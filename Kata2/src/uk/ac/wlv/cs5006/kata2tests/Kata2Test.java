package uk.ac.wlv.cs5006.kata2tests;

import static org.junit.Assert.assertTrue;
import org.junit.Test;
import javax.script.ScriptException;
import uk.ac.wlv.cs5006.kata2.Kata2;



public class Kata2Test {

    @Test
    public void testEvaluateStringLen() throws NumberFormatException, ScriptException {
        String s = " 1 + 2";
        assertTrue(Kata2.evaluate(s) == 0);
        s = " 1 + 2 +";
        assertTrue(Kata2.evaluate(s) == 0);
        s = null;
        assertTrue(Kata2.evaluate(s) == 0);
        s = "";
        assertTrue(Kata2.evaluate(s) == 0);
    }

    @Test
    public void testEvaluateNonNumeric() throws NumberFormatException, ScriptException {
        String s = "Ian";
        assertTrue(Kata2.evaluate(s) == 0);
        s = "Three + four + five";
        assertTrue(Kata2.evaluate(s) == 0);
    }

    @Test
    public void testEvaluateAdd() throws NumberFormatException, ScriptException {
        String s = "1 + 2 + 3";
        assertTrue(Kata2.evaluate(s) == 6);
        s = "4.2 + 5.63 + 1.0";
        assertTrue(Kata2.evaluate(s) == 10.83);
        s = "10000000 + 10000000 + 10000000";
        assertTrue(Kata2.evaluate(s) == 30000000);
    }

    @Test
    public void testEvaluateSub() throws NumberFormatException, ScriptException {
        String s = "1 - 2 - 1";
        assertTrue(Kata2.evaluate(s) == -2);
        s = "100 - 2 - 5";
        assertTrue(Kata2.evaluate(s) == 93);
        s = "7.5 - 2.3 - 1.0";
        assertTrue(Kata2.evaluate(s) == 4.2);
        s = "10000000 - 10000000 - 10000000";
        assertTrue(Kata2.evaluate(s) == -10000000);
    }

    @Test
    public void testEvaluateMul() throws NumberFormatException, ScriptException {
        String s = "2 * 3 * 2";
        assertTrue(Kata2.evaluate(s) == 12);
        s = "-2 * 3 * 2";
        assertTrue(Kata2.evaluate(s) == -12);
        s = "44 * 0 * 2";
        assertTrue(Kata2.evaluate(s) == 0);
    }

    @Test
    public void testEvaluateDiv() throws NumberFormatException, ScriptException {
        String s = "2 / 0 / 2";
        assertTrue(Kata2.evaluate(s) == 0);
        s = "5 / 2 / 1";
        assertTrue(Kata2.evaluate(s) == 2.5);
        s = "5 / 1 / 2";
        assertTrue(Kata2.evaluate(s) == 2.5);
    }

    @Test
    public void testEvaluateMixed() throws NumberFormatException, ScriptException {
        String s = "2 + 2 / 2";
        assertTrue(Kata2.evaluate(s) == 2);
        s = "10 + 2 - 5";
        assertTrue(Kata2.evaluate(s) == 7);
        s = "10 + 2 - 5 + 5";
        assertTrue(Kata2.evaluate(s) == 0);
        s = "10 * 2 - 5";
        assertTrue(Kata2.evaluate(s) == 15);
        s = "10 * 2 / 0";
        assertTrue(Kata2.evaluate(s) == 0);
        s = "0 /10 * 10";
        assertTrue(Kata2.evaluate(s) == 0);
        s = "100 + 50 * 10";
        assertTrue(Kata2.evaluate(s) == 1500);
        s = " 100 + 50 *10";
        assertTrue(Kata2.evaluate(s) == 0);
    }
}
