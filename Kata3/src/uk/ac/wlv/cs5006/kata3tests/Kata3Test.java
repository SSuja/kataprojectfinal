package uk.ac.wlv.cs5006.kata3tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import uk.ac.wlv.cs5006.kata3.Kata3;

public class Kata3Test {

    @Test
    public void testNull() {
        String morse = Kata3.translateToMorseCode(null);
        assertTrue(morse.length() == 0);
    }

    @Test
    public void testEmpty() {
        String morse = Kata3.translateToMorseCode("");
        assertTrue(morse.length() == 0);

        morse = Kata3.translateToMorseCode(" ");
        assertTrue(morse.length() == 0);
    }

    @Test
    public void testNumerals() {
        String morse = Kata3.translateToMorseCode("1");
        assertTrue(morse.length() == 0);

        morse = Kata3.translateToMorseCode("343434534534543");
        assertTrue(morse.length() == 0);

        morse = Kata3.translateToMorseCode("343434534534543434434324234324234324324234");
        assertTrue(morse.length() == 0);

        morse = Kata3.translateToMorseCode("Thatsanothernicemes5youvegottenmeinto");
    }

    @Test
    public void testMorse() {
        String morse = Kata3.translateToMorseCode("a");
        String tokens[] = morse.split(" ");

        assertFalse(morse.length() == 0);
        assertTrue(tokens[0].equals("._"));

        morse = Kata3.translateToMorseCode("ziggy");
        tokens = morse.split(" ");

        assertTrue(tokens[0].equals("__.."));
        assertTrue(tokens[1].equals(".."));
        assertTrue(tokens[2].equals("__."));
        assertTrue(tokens[3].equals("__."));
        assertTrue(tokens[4].equals("_.__"));

        morse = Kata3.translateToMorseCode("lolcat");
        tokens = morse.split(" ");

        assertTrue(tokens[0].equals("._.."));
        assertTrue(tokens[1].equals("___"));
        assertTrue(tokens[2].equals("._.."));
        assertTrue(tokens[3].equals("_._."));
        assertTrue(tokens[4].equals("._"));
        assertTrue(tokens[5].equals("_"));
    }

    @Test
    public void testEnglish() {
        assertTrue(Kata3.translateFromMorseCode(Kata3.translateToMorseCode("lolcat")).equals("lolcat"));
        assertTrue(Kata3.translateFromMorseCode(Kata3.translateToMorseCode(null)).equals(""));
        assertTrue(Kata3.translateFromMorseCode(Kata3.translateToMorseCode("")).equals(""));
        assertTrue(Kata3.translateFromMorseCode(Kata3.translateToMorseCode("hahaha")).equals("hahaha"));
        assertTrue(Kata3.translateFromMorseCode(Kata3.translateToMorseCode("abcdefghijklmnopqrstuvwxyz"))
                .equals("abcdefghijklmnopqrstuvwxyz"));
        assertTrue(Kata3.translateFromMorseCode(Kata3.translateToMorseCode("2")).equals(""));
    }
}
