package uk.ac.wlv.cs5006.kata3;


/**
 * 
 * @author SujamathyS.
 * @param string,word
 * @return double
 *
 */
public class Kata3 {
    // Array of all valid alpha characters
    private static String[] alphanumeric = 
        { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
        "n", "o","p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", " " };

    // Array of all valid Morse code characters
    private static String[] morsecode = 
        { "._", "_...", "_._.", "_..", ".", ".._.", "__.", "....", "..", ".___", "_._",
        "._..", "__", "_.", "___", ".__.", "__._", "._.", "...", "_", ".._", "..._", ".__",
        "_.._", "_.__", "__..","|" };

    /**
     * this method is translate string to morsecode.
     */
    public static String translateToMorseCode(String word) {
        if (word != null && word!=" " && !word.isEmpty() 
               && !word.matches("[0-9]+")) {
            String userInput = word.toLowerCase();

            char[] chars = userInput.toCharArray();

            String textInMorseCode = "";
            for (int i = 0; i < chars.length; i++) {
                for (int j = 0; j < alphanumeric.length; j++) {

                    if (alphanumeric[j].equals(String.valueOf(chars[i]))) {
                        textInMorseCode = textInMorseCode + morsecode[j] + " ";
                    }
                }
            }
            System.out.println(textInMorseCode);
            System.out.println("TESTM" + word.matches("[0-9]+"));
            return textInMorseCode;
        }
        return "";
    }

    /**
     * this method is translate Morsecode to String.
     */
    public static String translateFromMorseCode(String morsecodeSign) {
        String alphanumericWord = "";

        // isString function take word as first parameter and type as second parameter
        // for skip checking blank space in morse code
        if (isStringFine(morsecodeSign, 'M')) {
            String[] morsecodeArray = morsecodeSign.split(" ");
            for (int i = 0; i < morsecodeArray.length; i++) {
                for (int j = 0; j < morsecode.length; j++) {

                    if (morsecodeArray[i].equals(morsecode[j])) {

                        alphanumericWord = alphanumericWord + alphanumeric[j];
                    }
                }
            }
        }
        System.out.println(alphanumericWord);
        return alphanumericWord;
    }

    private static boolean isStringFine(String myString, char type) {
        if (myString != null && myString != "") {
            int stringLength = myString.length();

            for (int i = 0; i < stringLength; i++) {
                if (myString.charAt(i) == ' ' && type == 'A') {
                    return false;
                }
                if (Character.isDigit(myString.charAt(i))) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }

    }
}
