package uk.ac.wlv.cs5006.kata0;

/**
 * 
 * @author SujamathyS.
 * @param numberOne,numberOne
 * @return numberOne/numberOne
 */
public class Kata0 {

    /**
     * return maximum number of the two numbers which is input as parameter in this method.
     * 
     */
    public int max(int numberOne, int numberTwo) {
        if (numberOne > numberTwo) {
            return numberOne;
        } else if (numberTwo > numberOne) {
            return numberTwo;
        } else {
            return numberOne;
        }
    }
}
