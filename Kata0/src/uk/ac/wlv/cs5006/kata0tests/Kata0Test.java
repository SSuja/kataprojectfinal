package uk.ac.wlv.cs5006.kata0tests;

import junit.framework.Assert;
import org.junit.Test;
import uk.ac.wlv.cs5006.kata0.Kata0;

public class Kata0Test{

    Kata0 kata0 = new Kata0();
    
    @Test
    public final void testMaxPositive() {
        Assert.assertEquals(2, kata0.max(1, 2));
        Assert.assertEquals(2, kata0.max(2, 1));
    }

    @Test
    public final void testMaxNegative() {
        Assert.assertEquals(-1, kata0.max(-1, -2));
        Assert.assertEquals(-1, kata0.max(-2, -1));
    }

    @Test
    public final void testMaxSame() {
        Assert.assertEquals(2, kata0.max(2, 2));
        Assert.assertEquals(0, kata0.max(0, 0));
        Assert.assertEquals(-2, kata0.max(-2, -2));
    }

}